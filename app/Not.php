<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Not extends Model
{	
	protected $fillable = ['text'];
	
 	public function pages(){
   		return $this->belongsTo(Page::class);
   	}
}
