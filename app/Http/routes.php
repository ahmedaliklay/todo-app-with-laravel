<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','HomeController@indexFunc');
//Route::get('index',function(){return view('index');});

Route::get('index','HomeController@indexFunc');

Route::get('index/{page}','HomeController@showpages');

Route::post('addNew','HomeController@addNew');

Route::get('page/{id}/delete','HomeController@delete');

Route::get('/{id}/deleteall','HomeController@deleteall');

Route::post('pages/{page}/AddNewNots','HomeController@AddNewNots');

Route::get('Nots/{id}/delete','HomeController@deleteNots');

Route::get('Nots/{id}/edit','HomeController@EditNots');

Route::post('Nots/{id}/update','HomeController@updateNots');

Route::get('pages/{id}/edait','HomeController@updatePage');

Route::post('pages/{id}/update','HomeController@saveupdatePage');