<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\Page;

use App\Not;

class HomeController extends Controller
{
    public function indexFunc(){
    	$dataPages = DB::table('pages')->get();
    	return view('pages.index',compact('dataPages'));
    }

    public function showpages(Page $page){
        return view('pages.onepage',compact('page'));
    }

    public function addNew(Request $request){

        // $this->validate($request,[
        //     'title' => 'required|min:5|max:20'       
        // ],[
        //     'text.required' => 'يجب ملئ الحقل يااحمق',
        //     'text.min'      => 'لايجب ان يقل عدد الحروف عن 5'
        // ]);
    	$new = new Page;

    	$new->title = $request->title;
    	$new->save();
    	return back();

    }
    public function AddNewNots(Page $page,Request $request){
        $this->validate($request,[
            'text' => 'required|min:5|max:50'
        ],[
            'text.required' => 'يجب ملئ الحقل يااحمق',
            'text.min'      => 'لايجب ان يقل عدد الحروف عن 5'
        ]);
        $not = new Not;

        $not->text = $request->text;

        $page->nots()->save($not);

        return back();
    }

    public function delete(Page $id){
        if(count($id->nots)){
            return view('pages.deleteall',compact('id'));
        }else{
            $id->delete();
            return back();
        }
        return back();
        
    }
    public function deleteall(Page $id){
        $id->delete();
        $id->nots()->delete();
        return redirect('../../index');
        return back();
    }
    /*
 function delete(Page $id){
        if(count($id->nots)){
            return view('pages.deleteall',compact('id'));
        }else{
            $id->delete();
            return back();
        }
        
    }
    function deleteall(Page $id){
        $id->delete();
        return redirect('../../index');
    }
    */
    //////////////////////////////
    public function deleteNots(Not $id){
        $id->delete();
        return back();
    }
    public function EditNots(Not $id){

        return view('pages.edait',compact('id'));

    }
    public function updateNots(Request $request,Not $id){

        $id->update($request->all());

        return redirect('index/' . $id->page_id);
        //redirect('index' . $id->id)
    }
    public function updatePage(Page $id){

        return view('pages.edaitPage',compact('id'));

    }

    public function saveupdatePage(Request $request,Page $id){

        $id->update($request->all());

        return redirect('index');

    }
}


// class HomeController extends Controller
// {
//     function indexFunc(){
//     	$name = "Ahmed Ali KLay From Laravel";
//     	$arrays = array('1','2','3');
//     	$dataPages = DB::table('pages')->get();
//     	return view('pages.index',['name'=>$name,'arrays'=>$arrays,'dataPages'=>$dataPages]);
//     }
// }
